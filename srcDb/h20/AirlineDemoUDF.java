

import hex.genmodel.easy.RowData;
import hex.genmodel.easy.EasyPredictModelWrapper;
import hex.genmodel.easy.prediction.*;

/**
 * VoltDB user defined function for H2o. Note that all of the 'intelligence' is in the method 'ademo'.
 * 
 * @author drolfe
 *
 */
public class AirlineDemoUDF {
 
    /**
     * Name of h20.ai class we're going to use.
     */
    private static String modelClassName = "gbm_pojo_test";

    /**
     * This VoltDB User Defined Function uses an H20.AI function to guess whether a given
     * flight will be late. This example uses a US Federal Aviation Administration dataset of
     * commercial flight data.
     * 
     * To make it usable in VoltDB you create it as a SQL function:
     * <br>
     * <code>
     * CREATE FUNCTION ademo FROM METHOD AirlineDemoUDF.ademo; 
     * </code>
     * 
     * @param cRSDepTime
     *            Depature time
     * @param year
     *            year
     * @param month
     *            Month
     * @param dayOfMonth
     *            Day
     * @param dayOfWeek
     *            Day of week
     * @param uniqueCarrier
     *            Airline
     * @param origin
     *            Origin Airport
     * @param dest
     *            Destination Airport
     * @return pmmlOut An array of VoltTable objects containing the results.
     * @throws VoltAbortException
     */
    public String ademo(String cRSDepTime, String year, String month, String dayOfMonth, String dayOfWeek,
            String uniqueCarrier, String origin, String dest) {

        try {

            // Instantiate model...
            hex.genmodel.GenModel rawModel;
            rawModel = (hex.genmodel.GenModel) Class.forName(modelClassName).newInstance();
            EasyPredictModelWrapper model = new EasyPredictModelWrapper(rawModel);
 
            // Load params into a H20 data structure...
            RowData row = new RowData();
            row.put("Year", year);
            row.put("Month", month);
            row.put("DayofMonth", dayOfMonth);
            row.put("DayOfWeek", dayOfWeek);
            row.put("CRSDepTime", cRSDepTime);
            row.put("UniqueCarrier", uniqueCarrier);
            row.put("Origin", origin);
            row.put("Dest", dest);
            
            // Call model...
            BinomialModelPrediction p = model.predictBinomial(row);
            
            // Return the most obvious result...
            return (p.label);

        } catch (Exception e) {

            System.err.println(e.getMessage());
            return null;

        }

    }

}