import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.voltdb.client.Client;
import org.voltdb.client.ClientConfig;
import org.voltdb.client.ClientFactory;
import org.voltdb.client.ClientResponse;

public class Demo {

    public static void main(String[] args) {

        try {
            msg("start ");

            int nacount = 0;
            int loadcount = 0;

            Client c = connectVoltDB("127.0.0.1");

            List<String> theList;
            theList = Demo.loadFile(args[0]);

            String[][] values = new String[theList.size()][];

            for (int i = 0; i < theList.size(); i++) {
                String[] lineValues = theList.get(i).split(",");
                values[i] = lineValues;

                for (int j = 0; j < values[i].length; j++) {
                    values[i][j] = values[i][j].replace("\"", "");
                }
            }

            msg("Insert " + theList.size() + " rows of flight data");

            for (int i = 1; i < theList.size(); i++) {

                // System.out.println(i);
                if (!values[i][4].equals("NA")) {
                    try {

                        loadcount++;

                        c.callProcedure("flights.UPSERT", Long.parseLong(values[i][0]), Long.parseLong(values[i][1]),
                                Long.parseLong(values[i][2]), Long.parseLong(values[i][3]), values[i][4], values[i][5],
                                values[i][6], values[i][7], values[i][8], Long.parseLong(values[i][9]), values[i][10],
                                values[i][16], values[i][17]);
                    } catch (Exception e) {
                        System.out.print(e.getMessage() + " " + i + " ");
                        for (int z = 0; z < values[i].length; z++) {
                            System.out.print(values[i][z] + " ");
                        }
                        System.out.println("");
                    }
                } else {
                    nacount++;
                }
            }

            msg("Loaded " + loadcount + ", Skipped = " + nacount);
            nacount = 0;

//            for (int i = 1; i < theList.size(); i++) {
//                 ClientResponse cr;
//
//                if (!values[i][4].equals("NA")) {
//                    try {
//                        cr = c.callProcedure("AirlineDemo", values[i][4], values[i][0], values[i][1], values[i][2],
//                                values[i][3], values[i][8], values[i][16] // orig
//                                , values[i][17] // dest
//                        );
//                    } catch (Exception e) {
//                        System.out.print(e.getMessage() + " " + i + " ");
//                        for (int z = 0; z < values[i].length; z++) {
//                            System.out.print(values[i][z] + " ");
//                        }
//                        System.out.println("");
//                    }
//                } else {
//
//                }
//
//                nacount++;
//            }

            for (int i = 1; i < theList.size(); i += 100) {
                // String cRSDepTime, String year, String month, String
                // dayOfMonth, String dayOfWeek,String uniqueCarrier, String
                // origin, String dest
                // System.out.println(values[i]);
                ClientResponse cr = c.callProcedure("flight_hist", values[i][9]);

                System.out.println(i + System.lineSeparator() + " " + cr.getResults()[0].toFormattedString());
            }

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static List<String> loadFile(String filename) throws IOException {

        BufferedReader in = null;
        List<String> list = new ArrayList<String>();
        try {
            in = new BufferedReader(new FileReader(filename));
            String str;
            while ((str = in.readLine()) != null) {
                list.add(str);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                in.close();
            }
        }
        // Scanner s = new Scanner(new File(filename));
        // ArrayList<String> list = new ArrayList<String>();
        // while (s.hasNext()) {
        // list.add(s.next());
        // }
        // s.close();
        return list;
    }

    private static Client connectVoltDB(String hostname) throws Exception {
        Client client = null;
        ClientConfig config = null;

        try {
            // msg("Logging into VoltDB");

            config = new ClientConfig(); // "admin", "idontknow");
            config.setMaxOutstandingTxns(20000);
            config.setMaxTransactionsPerSecond(200000);
            config.setTopologyChangeAware(true);
            config.setReconnectOnConnectionLoss(true);

            client = ClientFactory.createClient(config);

            client.createConnection(hostname);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("VoltDB connection failed.." + e.getMessage(), e);
        }

        return client;

    }

    public static void msg(String message) {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        System.out.println(strDate + ":" + message);
    }

}
