import java.io.*;
import hex.genmodel.easy.RowData;
import hex.genmodel.easy.EasyPredictModelWrapper;
import hex.genmodel.easy.prediction.*;

public class RunModel {
  private static String modelClassName = "gbm_pojo_test";

  public static void main(String[] args) throws Exception {
      
      for (int z=0; z < 10000; z++) {
    hex.genmodel.GenModel rawModel;
    long startMs = System.currentTimeMillis();
    rawModel = (hex.genmodel.GenModel) Class.forName(modelClassName).newInstance();
    EasyPredictModelWrapper model = new EasyPredictModelWrapper(rawModel);
    System.out.println("ms=" + (System.currentTimeMillis() - startMs));

    RowData row = new RowData();
    row.put("Year", "1987");
    row.put("Month", "10");
    row.put("DayofMonth", "14");
    row.put("DayOfWeek", "3");
    row.put("CRSDepTime", "730");
    row.put("UniqueCarrier", "PS");
    row.put("Origin", "SAN");
    row.put("Dest", "SFO");
    startMs = System.currentTimeMillis();
    BinomialModelPrediction p = model.predictBinomial(row);
    System.out.println("ms=" + (System.currentTimeMillis() - startMs));

    System.out.println("Label (aka prediction) is flight departure delayed: " + p.label);
    System.out.print("Class probabilities: ");
    for (int i = 0; i < p.classProbabilities.length; i++) {
      if (i > 0) {
        System.out.print(",");
      }
      System.out.print(p.classProbabilities[i]);
    }
    System.out.println("");
      }
  }
}