
load classes ../../h20-first.jar;

create table flights (
f_Year int not null, 
f_Month int not null, 
f_DayofMonth int not null, 
f_DayOfWeek int not null, 
f_DepTime varchar(4) not null, 
f_CRSDepTime varchar(4) not null, 
f_ArrTime  varchar(4) not null,  
f_CRSArrTime varchar(4) not null , 
f_UniqueCarrier varchar(3) not null , 
f_FlightNum int not null, 
f_TailNum varchar(8) not null,
f_origin varchar(3) not null,
f_dest varchar(3) not null,
constraint f_pk primary key (f_Year,f_Month,f_DayofMonth,f_DepTime,f_FlightNum));

partition table flights on column f_FlightNum;

create index f_ix1 on flights(f_FlightNum,f_Year,f_Month,f_DayofMonth);

CREATE PROCEDURE 
   FROM CLASS h20.AirlineDemo;
   
CREATE FUNCTION ademo FROM METHOD h20.AirlineDemoUDF.ademo;  

CREATE PROCEDURE flight_hist
PARTITION ON TABLE   flights COLUMN  f_FlightNum AS 
SELECT f_cRSDepTime,  f_year,  f_month,  f_dayOfMonth,  f_dayOfWeek, f_uniqueCarrier,  f_origin,  f_dest
, ademo(f_cRSDepTime,  f_year,  f_month,  f_dayOfMonth,  f_dayOfWeek, f_uniqueCarrier,  f_origin,  f_dest ) ademo
from flights
where f_FlightNum = ?
order by f_year,  f_month,  f_dayOfMonth,f_cRSDepTime;