load classes  ../../jpmml-util.jar;

CREATE PROCEDURE
   FROM CLASS jppml.GolfDemo;

CREATE FUNCTION tree FROM METHOD jppml.generated.TreeProcedure.tree;

exec GolfDemo 1 1 false sunny;

exec GolfDemo 1 1 false sunny;


create table foo (foocol int);

insert into foo values (30);
insert into foo values (40);
insert into foo values (50);
insert into foo values (60);
insert into foo values (70);
insert into foo values (80);


select foocol, tree(foocol, 1,'false','sunny') from foo;

load classes  ../../jpmml-util.jar;


create table auditdata (ID  int primary key not null
,Age	 int 
,Employment	 varchar(30)
,Education	 varchar(30)
, Marital	 varchar(30)
,Occupation	 	varchar(30)
,Income	  float
,	Gender	    	varchar(30)
,Deductions		 float
,Hours		 float
,IGNORE_Accounts		 varchar(30)
,RISK_Adjustment		 varchar(30)
,TARGET_Adjusted		 varchar(30));

partition table auditdata on column id;

CREATE FUNCTION auditTree FROM METHOD jppml.generated.AuditTreeProcedure.auditTree;
